#
# Be sure to run `pod lib lint SwiftNDNTouch.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SwiftNDNTouch'
  s.version          = '0.2.1'
  s.summary          = 'This is NDN-CXX equivalent implementation in Swift on the iOS platform'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
This implementation is based on NDN-CXX, but written in Swift for the iOS platform developers. Different than other platforms, there is no NFD running in the background, so a developer should specify forwarder's IP address when creating a face. The details can be found in the example. Also, it requires further efforts on implementing security parts that are not yet implemented since the main focus for now is data transmission.
                       DESC

  s.homepage         = 'https://bitbucket.org/jdlee6461/swiftndntouch'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Wentao Shang' => 'wentaoshang@ucla.edu', 'Jongdeog Lee' => 'jlee700@illinois.edu' }
  s.source           = { :git => 'https://bitbucket.org/jdlee6461/swiftndntouch', :tag => s.version.to_s }
  # s.source           = { :path => '/Users/Jongdeog/Documents/SwiftNDNTouch' }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '10.0'

  s.source_files = 'SwiftNDNTouch/Classes/**/*'
  
  # s.resource_bundles = {
  #   'SwiftNDNTouch' => ['SwiftNDNTouch/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'CocoaAsyncSocket','~> 7.5.0'
end
