//
//  ViewController.swift
//  SwiftNDNTouch
//
//  Created by Jongdeog Lee on 12/05/2016.
//  Copyright (c) 2016 Jongdeog Lee. All rights reserved.
//

import UIKit
import SwiftNDNTouch

class MasterViewController: UIViewController, TrivialConsumerDelegate {

    @IBOutlet weak var outputText: UILabel!
    
    var consumer: TrivialConsumer?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let prefix = "/test"
        consumer = TrivialConsumer(delegate: self, prefix: prefix, forwarderIP: "128.174.246.10", forwarderPort: 6363)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func sendInterest(_ sender: UIButton) {
        consumer?.get(postfix: "/hello")
    }

    // Delegate functions
    
    func onData(i: Interest, d: SwiftNDNTouch.Data) {
        let content = d.getContent()
        let buffer = NSString(bytes: content, length: content.count, encoding: String.Encoding.utf8.rawValue)!
        print (buffer)
        outputText.text = buffer as String
    }
    
    func onOpen() {
        print("Trivial Consumer connected!")
    }

    func onError(reason: String) {
        print("Trivial Consumer Error caused by "+reason)
    }

    func onClose() {
        print("Trivial Consumer close!")
    }
    
}

