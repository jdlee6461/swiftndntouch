# SwiftNDNTouch

## [![CI Status](http://img.shields.io/travis/Jongdeog Lee/SwiftNDNTouch.svg?style=flat)](https://travis-ci.org/Jongdeog Lee/SwiftNDNTouch)
[![Version](https://img.shields.io/cocoapods/v/SwiftNDNTouch.svg?style=flat)](http://cocoapods.org/pods/SwiftNDNTouch)
[![License](https://img.shields.io/cocoapods/l/SwiftNDNTouch.svg?style=flat)](http://cocoapods.org/pods/SwiftNDNTouch)
[![Platform](https://img.shields.io/cocoapods/p/SwiftNDNTouch.svg?style=flat)](http://cocoapods.org/pods/SwiftNDNTouch)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SwiftNDNTouch is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SwiftNDNTouch"
```

## Author

Wentao Shang, wentaoshang@ucla.edu
Jongdeog Lee, jdlee6461@gmail.com

## License

SwiftNDNTouch is available under the MIT license. See the LICENSE file for more info.
